var gulp = require('gulp'),
    sass = require('gulp-sass'),
    compass = require('gulp-compass'),
    path = require('path'),
    minifyCSS = require('gulp-minify-css'),
    livereload = require('gulp-livereload'),
    autoprefixer = require('gulp-autoprefixer'),
    gcmq = require('gulp-group-css-media-queries'),
    concat = require('gulp-concat');

var myStyles = [
    './sass/variables.scss',
    './sass/reset.scss',
    './sass/mixins.scss',
    './sass/main.scss',
    './sass/part-header.scss',
    './sass/work.scss',
    './sass/advantages.scss',
    './sass/footer.scss',
    './sass/make-order.scss',
    './sass/statistic.scss',
    './sass/order.scss',
    './sass/header.scss',
    './sass/banner.scss',
    './sass/carousel.scss',
    './sass/modal.scss',
    './sass/gallery.scss',
    './sass/fixed-menu.scss',
    './sass/form.scss',
];

gulp.task('default',["watch"], function() {
    // 1. concat scss
    // 2. concat media queries
    // 3. add auto prefixes
    // 4. build and minify css
    // 5. copy style.css to /css/
    // 6. watch changes scss
});

gulp.task('concat-scss', function() {
    return gulp.src(myStyles)
        .pipe(concat('style.scss'))
        .pipe(gulp.dest('./sass/'));
});

gulp.task('compass', function() {
    gulp.src('./sass/style.scss')
        .pipe(compass({
            css: 'css',
            sass: 'sass'
        }))
        .pipe(autoprefixer(
            {
                browsers: [
                    '> 1%',
                    'last 2 versions',
                    'firefox >= 4',
                    'safari 7',
                    'safari 8',
                    'IE 10',
                    'IE 11'
                ],
                cascade: false
            }
        ))
        .pipe(gcmq())
       // .pipe(minifyCSS())
        .pipe(gulp.dest('css'))
        .pipe(livereload());
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(myStyles, ['concat-scss','compass']);
});