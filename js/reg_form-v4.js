(function($){

var rv_name = /^[a-zA-Zа-яА-Я0-9_\.\-\ ]+$/;
var rv_email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9_\.\-])+\.)+([a-zA-Z0-9]{2,6})+$/;
var rv_phone = /^(\d|\+)[\d\(\)\ +-]{4,14}\d$/;

var wrappers = $('.form-wrapper');

$(document).ready(function(){
    forms_init();
    // form_modal_init();
    // modal_add();
    // modal_form_init();

    form_answer_centerize();
    form_status_close();
});


$(window).resize(function (){
    form_answer_centerize();
});


// function modal_form_init() {
//     var lnk_href = window.location.href;//current url
//     $('input[name="url"]').val(lnk_href);
//
//     var link = $('.js-modal-form'),
//         pp_texts = $('.pipo-contacts-text');
//
//     link.each(function(){
//         var lnk = $(this),
//             //lnk_href = lnk.data('href'),
//             lnk_text = lnk.attr('data-text'),
//             lnk_short = lnk.attr('data-short');
//         lnk.on('click', function(){
//             $(lnk_text).show();
//             $('#msg_short2').val(lnk_short);
//         });
//     });
//
//     $('#pipo-contacts')
//         .on('hide.bs.modal', function (e) {
//             $('#pipo-contacts #msg_short2').val('');
//             pp_texts.hide();
//         });
//     //.on('show.bs.modal', function (e) {});
// };

function forms_init(){

    wrappers.each(function() {

        var fwrap = $(this);
        var fform = fwrap.find('.form-flex');
        var fanws = fwrap.find('.form-answer');
        fform.bind('submit', function () {
            var name = fform.find('input[name="name"]');
            var name_val = name.val();
            var phone = fform.find('input[name="phone"]');
            var phone_val = phone.val();
            var email = fform.find('input[name="email"]');
            var email_val = email.val();
            var validation = true;

            var phone_val_test = phone_val.replace("(","");
            phone_val_test = phone_val_test.replace(")","");
            phone_val_test = phone_val_test.replace("+","");
            phone_val_test = phone_val_test.replace("-","");
            phone_val_test = phone_val_test.replace("-","");
            phone_val_test = phone_val_test.replace("-","");
            phone_val_test = phone_val_test.replace(" ","");
            phone_val_test = phone_val_test.replace(" ","");
            phone_val_test = phone_val_test.replace(" ","");
            phone_val_test = phone_val_test.replace(" ","");
            // phone_val = phone_val_test;

            if (phone_val != '' && rv_phone.test(phone_val_test)) {
                phone.parent().removeClass('has-error');
            } else {
                phone.parent().addClass('has-error');
                validation = false;
            };

            if (name_val.length > 2 && name_val != '' && rv_name.test(name_val)) {
                name.parent().removeClass('has-error');
            } else {
                name.parent().addClass('has-error');
                validation = false;
            };

            if (email.length) {
                if ((email_val != '' && rv_email.test(email_val)) || email_val == '') {
                    if (email_val == '' && phone_val_test == '74951341801') {
                        email.parent().addClass('has-error');
                        validation = false;
                    } else {
                        email.parent().removeClass('has-error');
                    };
                } else {
                    email.parent().addClass('has-error');
                    validation = false;
                };
            };

            if (!validation) {
                return false;
            };

            var msg = $(this).serialize();
            var btn = fform.find('.btn[type="submit"]');
            $.ajax({
                type: "POST",
                url: "./reg_web-v4a.html",
                data: msg,
                beforeSend: function () {
                    console.info('v4a-db start');
                    btn.attr('disabled', 'disabled');
                    form_answer_centerize();
                    // $.ajax({
                    //     type: "POST",
                    //     url: "/reg_web-v4b.php",
                    //     data: msg
                    //     ,
                    //     beforeSend: function () {
                    //         console.info('v4b-crm start');
                    //     },
                    //     success: function (data) {
                    //         console.info('v4b-crm end success');
                    //         console.info(data);
                    //     },
                    //     error: function (xhr, str) {
                    //         console.info('v4b-crm end error');
                    //         console.info(xhr);
                    //         console.info(str);
                    //     }
                    // });
                },
                success: function (data) {
                    console.info('v4a-db end success');
                    console.info(data);
                    if (data.indexOf('success') + 1) {
                        fanws.find('.form-is-success').show();
                    } else {
                        fanws.find('.form-is-error').show();
                        fwrap.find('.js-form-dangers').html(data);
                    }

                    fwrap.addClass('toggle');

                    btn.removeAttr('disabled');
                    fform.trigger('reset').find('.has-error').removeClass('.has-error');
                    //add_form_info();
                },
                error: function (xhr, str) {
                    console.info('v3a-db end error');
                    console.info(xhr);
                    console.info(str);
                    alert("Возникла ошибка! Пожалуйста, попробуйте еще раз.");
                    btn.removeAttr('disabled', false);
                }
            });
        });

    });
}

// function modal_add(){
//     $('#pipo-contacts')
//         .on('show.bs.modal', function (e) {
//             form_answer_centerize();
//         }).on('hide.bs.modal', function (e) {
//             // $('#form-wrapper2').removeClass('toggle');
//             // $('#results2').find('.form-is-error').hide();
//             // $('#results2').find('.form-is-success').hide();
//             $('.form-wrapper').removeClass('toggle')
//                 .find('.form-is-error').hide()
//                 .end()
//                 .find('.form-is-success').hide();
//         });
// };



function form_status_close() {
    wrappers.each(function(){
        var fwrap = $(this);
        // var fform = fwrap.find('.form-flex');
        var fanws = fwrap.find('.form-answer');
        fwrap.find('.text-block-close').on('click',function(){
            fwrap.removeClass('toggle');
            fanws.find('.form-is-error').hide();
            fanws.find('.form-is-success').hide();
        });
    });
};

function form_answer_centerize(){
    wrappers.each(function(){
        var fwrap = $(this);
        var fform = fwrap.find('.form-flex');
        var fanws = fwrap.find('.form-answer');
        var height = fform.outerHeight();
        fanws.css({
            'min-height': height,
            'height': height,
            'margin-top': -height
        });
    });
};

})(jQuery);
