"use strict";

function addEventToStatisticHover(){
    var scrollSpeed = 300;
    $('.statistic>.item').on("mouseenter",function (a) {
        var h = a.currentTarget;
        h= h.getElementsByClassName('hover');
        if(h.length) {
            h=$(h[0]);
            h.show();
            h.animate({top: '0px'},scrollSpeed, function() {});
        }
    });
    $('.statistic>.item').on("mouseleave",function (a) {
        var h = a.currentTarget;
        h= h.getElementsByClassName('hover');
        if(h.length) {
            h=$(h[0]);
            h.animate({top: '-500px'},scrollSpeed,function() {h.hide()});
        }
    });
}

function startAnimationDownElement(){
    setInterval(function(){
        $(".down").animate({bottom: "+=20"}, 1500, function() { });
        $(".down").animate({bottom: "-=20"}, 1500, function() { });
    }, 1000)
}

function initCarousel(){
    $("#owl-feeds").owlCarousel({
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        navigation: false,
    });
}

function menuToogleOnScroll() {
    var flag = true;
    var fm = $(".fixed-menu");
    var scrollHeight = 600;
    var scrollSpeed = 500;
    var scrollDown = false;
    var lastScrollPos = 0;
    window.onscroll = function(){
        var scrollPos = document.body.scrollTop;
        if(scrollPos>=lastScrollPos){
            scrollDown = true;
        } else {
            scrollDown = false;
        }
        lastScrollPos = scrollPos;
        if (((scrollPos > scrollHeight) && (flag)) && (scrollDown)) {
            flag = false;
            fm.show();
            fm.animate({top: "0"}, scrollSpeed);
        }
        if (((scrollPos <= scrollHeight) && (!flag)) || (!scrollDown && (!flag))) {
            fm.animate({top: "-100"},scrollSpeed,function(){
                fm.hide();
            });
            flag = true;
        }
    };
}

function addClickEvents() {
    $('.modal-wrap>.overlay').click(function () {
        $('.modal-wrap').hide("slow");
    });

    $('.modal-wrap .close').click(function () {
        $('.modal-wrap').hide("slow");
    });

    $('.fixed-menu .submit').click(function () {
       $('.modal-wrap').show("fast");
    })
}

function initGalerry() {
    $('body').append('<div class="modal-wrap-gallery"><div class="window"> <div class="image-wrap"></div> <div class="close"><span class="flaticon-multiply"></span></div> <div class="prev"><span class="flaticon-arrows"></span></div> <div class="next"><span class="flaticon-arrows-1"></span></div> </div> <div class="overlay"></div> </div>');
    $('.modal-wrap-gallery>.overlay').click(function () {
        $('.modal-wrap-gallery').hide("slow");
    });

    $('.modal-wrap-gallery .close').click(function () {
        $('.modal-wrap-gallery').hide("slow");
    });

    $("body").keydown(function(e) {
        if(e.keyCode == 37) { // left
            console.log($('.modal-wrap-gallery').css('display'))
        }
        else if(e.keyCode == 39) { // right
        }
    });

    var scrollSpeed = 300;
    $('.gallery>.gallery-line>.item').append("<div class='hover' style='display: none'><div class='text'><span class='flaticon-search'>ПРОСМОТРЕТЬ</span></div></div>");
    $('.gallery>.gallery-line>.item').on("mouseenter",function (a) {
        var h = a.currentTarget;
        h= h.getElementsByClassName('hover');
        if(h.length) {
            h=$(h[0]);
            h.show();
            h.animate({top: '0px'},scrollSpeed, function() {});
        }
    });
    $('.gallery>.gallery-line>.item').on("mouseleave",function (a) {
        var h = a.currentTarget;
        h= h.getElementsByClassName('hover');
        if(h.length) {
            h=$(h[0]);
            h.animate({top: '-500px'},scrollSpeed, function(){h.hide()});
        }
    });

    var picArr = [];
    var index = -1;

    $('.modal-wrap-gallery .next').click(function(){
        if(picArr.length && index>-1){
            index++;
            if(index>picArr.length-1){
                index=0;
            }
            setImgModal(picArr[index]);
        }
    });
    $('.modal-wrap-gallery .prev').click(function(){
        if(picArr.length && index>-1){
            index--;
            if(index<0){
                index=picArr.length-1;
            }
            setImgModal(picArr[index]);
        }
    });

    function setImgModal(pic){
        if(pic.imgSrc){
            var winWidth = $(window).width();
            var winHeight = $(window).height();
            var windowWidth = $('.modal-wrap-gallery .window').width();
            var windowHeight = $('.modal-wrap-gallery .window').height();

            var dW= winWidth - 200;
            var dH= winHeight - 100;

            if(winWidth<768){
                dW= winWidth - 40;
            }

            var pW = pic.width || 800;
            var pH = pic.height || 600;

            var iW = dW;
            var iH = dH;

            console.log('dW',dW);
            console.log('dH',dH);

            console.log('pW',pic.width);
            console.log('pH',pic.height);

            var kW = dW/pW;
            var kH = dH/pH;
            if(kW>=kH){
                iW = pW*kH;
                iH = pH*kH;
            } else {
                iW = pW*kW;
                iH = pH*kW;
            }

            console.log('iW',iW);
            console.log('iH',iH);

           $('.modal-wrap-gallery .window').css('width',iW);
           $('.modal-wrap-gallery .window').css('height',iH);
           $('.modal-wrap-gallery .image-wrap').css('background','url("'+pic.imgSrc+'")');
           $('.modal-wrap-gallery .image-wrap').css('background-size','cover');
           $('.modal-wrap-gallery .image-wrap').css('background-position','center');
           $('.modal-wrap-gallery .image-wrap').css('background-repeat','no-repeat');
        }
    }

    function findInArray(array, imgSrc){
        for(var i=0; i<array.length; i++){
            if(array[i].imgSrc == imgSrc){
                return i;
            }
        }
        return -1;
    }

    function reindentGallery(){
        $('.gallery').each(function(galleryIndex, item){
            var galleryWrap = $(item).parent();
            var lines = $(item).children();
            var picArray = [];
            var selected = 0;
            lines.each(function(i, item){
                var lineHeight = 0;
                var line = $(item).children();
                line.each(function(i, item){
                    var type = $(item);
                    if(!lineHeight) {
                        lineHeight = type.width();
                    } else {
                        if(lineHeight>type.width()){
                            lineHeight = type.width();
                        }
                    }
                });
                line.each(function(i, item){
                    var type = $(item);
                    type.css("height",lineHeight);
                    selected++;
                    var imgSrc = type.children().attr('src');
                    var img = type.children();
                        picArray.push({
                            "imgSrc": imgSrc,
                            "width": img.width(),
                            "height": img.height()
                        });
                    type.css("cursor","pointer");
                    type.click(function () {
                        $('.modal-wrap-gallery').show(scrollSpeed);
                        picArr = picArray;
                        index = findInArray(picArray,imgSrc);
                        setImgModal(picArr[index]);
                    });
                    img.hide();
                    type.css("background","url('"+ imgSrc+"')");
                    type.css("background-size","cover");
                });
            });
        });
    }
    reindentGallery();
    $(window).resize(reindentGallery);
}

function initCountdown(){
    if ($('.js-countdown').length) {
        var countdown = $('.js-countdown');
        var two = function (a) {
            return (9 < a ? "" : "0") + a
        };
        var formatTime = function(a) {
            a = Math.floor(a / 1E3);
            var b = Math.floor(a / 60),
                c = Math.floor(b / 60),
                d = c / 24 | 0,
                c = c % 24;
            a %= 60;
            b %= 60;
            var time_string = "";
            if (d) {time_string = time_string + d + "" + days(d) + " ";}
            time_string = time_string + "<span>" + two(c) + ":</span>";// + hours(c) + " ";
            time_string = time_string + "<span>" + two(b) + ":</span>";// + minutes(b) + " ";
            time_string = time_string + "<span>" + two(a) + "</span>";// + " " + seconds(a);
            return time_string;
        };

        var plural = function (str1,str2,str5){
            return function ( n ) {return ((((n%10)==1)&&((n%100)!=11))?(str1):(((((n%10)>=2)&&((n%10)<=4))&&(((n%100)<10)||((n%100)>=20)))?(str2):(str5)))}
        };

        if(document.location.host=='en.payin-payout.net'){
            var days =  plural('D', 'D', 'D'),
                hours = plural('H', 'H', 'H'),
                minutes = plural('M', 'M', 'M'),
                seconds = plural('S', 'S', 'S');
        }else{
            var days =  plural('�', '�', '�'),
                hours = plural('�', '�', '�'),
                minutes = plural('�', '�', '�'),
                seconds = plural('�', '�', '�');
        }

        var Time = function() {
            var data = Date.parse('01/01/2014');
            data = new Date(data)
            for (; (new Date).getTime() > data; )  {
                data.setDate(data.getDate()+1)
            }
            var a = data, m = -180 - a.getTimezoneOffset();
            a.setMinutes(m, 0, 0);
            var a = a.getTime() -  (new Date).getTime();
            countdown.html(formatTime(a));
            if (a > 0) {
                window.setTimeout(Time, 1E3);
            } else {
                return false;
            };
        };
        Time();
    };
};

$(document).ready(function(e) {
    addEventToStatisticHover();
    startAnimationDownElement();
    initCarousel();
    menuToogleOnScroll();
    addClickEvents();
    initGalerry();
    initCountdown();
});
